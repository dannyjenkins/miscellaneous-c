// to compile:
// gcc snow.c -lncurses

#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>

// random number generator
int randgen(int l, int r) {
	return (rand() % (r - l + 1)) + l;
}

int main(int argc, char* argv[]) {

	initscr(); // start ncurses
  curs_set(0); // hide cursor
	scrollok(stdscr, TRUE); // proper scrolling behavior

	// get terminal size
	int max_rows, max_cols;
	getmaxyx(stdscr, max_rows, max_cols);

	char* the_chars[30];

	// lowest and highest index value of the_chars, used as range for
	// the random number generator
	int lower = 0;
	int upper = sizeof(the_chars) / sizeof(the_chars[0]) - 1;

	// This program defaults to displaying snow. However, if the -r
	// option is enabled, rain will be displayed instead, by using a
	// different set of characters and a different sleep interval.
	int rain = 0;
	for (int argument = 0; argument < argc; argument++) {
		if (strcmp("-r", argv[argument]) == 0) {
			rain = 1;
			break;
		}
	}

	// characters that will make up the precipitation
	if (rain) {
		the_chars[0] = "!";
		the_chars[1] = "|";
		the_chars[2] = "'";
		the_chars[3] = ".";
	}
	else {
		the_chars[0] = "*";
		the_chars[1] = ".";
		the_chars[2] = "'";
		the_chars[3] = ",";
	}

	for (int i = 4; i < 30; i++) the_chars[i] = " ";

	// string to be printed, consisting of randomly chosen characters
	// from the_chars
	char the_string[max_cols];

	// use current time as seed for random number generator
	srand(time(0));

	int sleep_time = rain ? 20 : 50;

	while (1) {
		strcpy(the_string, ""); // clear the string

		// generate new random string
		for (int i = 0; i < max_cols; i++) {
			strcat(the_string, the_chars[randgen(lower, upper)]);
		}

		mvprintw(0, 0, "%s", the_string);
		refresh();
		scrl(-1); // new output goes above previous output
		napms(sleep_time); // wait
	}

  endwin(); // exit ncurses
	return 0;
}