// to compile:
// gcc pong.c -lncurses

#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>

// random number generator
int randgen(int l, int r) {
	return (rand() % (r - l + 1)) + l;
}

int main() {

	initscr(); // start ncurses
   	curs_set(0); // hide cursor

	// get terminal size
	int max_rows, max_cols;
	getmaxyx(stdscr, max_rows, max_cols);

	// use current time as seed for random number generator
	srand(time(0));

	// the coordinates on which the next character will be printed
	int print_row = randgen(0, max_rows - 1);
	int print_col = randgen(0, max_cols - 1);

	// the values that will be added to print_row and print_col before
	// the next iteration in the loop
	int next_row_value = randgen(0, 1);
	int next_col_value = randgen(0, 1);
	if (next_row_value == 0) next_row_value = -1;
	if (next_col_value == 0) next_col_value = -1;

	char* the_char = "0"; // the character that will be printed

	int sleep_time = 10;

	while (1) {
		mvprintw(print_row, print_col, "%s", the_char);
		refresh();
		napms(sleep_time); // wait

		if (print_row == 0)            next_row_value = 1;
		if (print_col == 0)            next_col_value = 1;
		if (print_row == max_rows - 1) next_row_value = -1;
		if (print_col == max_cols - 1) next_col_value = -1;

		print_row += next_row_value;
		print_col += next_col_value;
	}

   	endwin(); // exit ncurses
	return 0;
}