#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h> // required for atoi

int main(int argc, char* argv[]) {

	if (argc < 2) {
		printf("ERROR: Please provide an integer argument.\n");
    return 1;
	}

	if (argc > 2) {
		printf("ERROR: Too many arguments.\n");
    return 1;
	}

	// atoi used to turn the command-line argument into an int

  int input_length = strlen(argv[1]);

  for (int i = 0; i < input_length; i++) {
    if (!isdigit(argv[1][i])) {
      printf("ERROR: Input not a valid integer.\n");
      return 1;
    }
  }

	int blocks_left  = atoi(argv[1]);
	int minutes_left = blocks_left * 10;
	int hours_left   = 0;
	int days_left    = 0;
	int years_left   = 0;

	while (minutes_left >= 60) {
		hours_left++;
		minutes_left -= 60;
	}

	while (hours_left >= 24) {
		days_left++;
		hours_left -= 24;
	}

	while (days_left >= 365) {
		years_left++;
		days_left -= 365;
	}

	if (years_left != 0) {
		if (days_left == 0 && hours_left == 0 && minutes_left == 0) {
			if (years_left == 1) printf("%d year", years_left);
			else printf("%d years", years_left);
		}
		else {
			if (years_left == 1) printf("%d year, ", years_left);
			else printf("%d years, ", years_left);
		}
	}

	if (days_left != 0) {
		if (hours_left == 0 && minutes_left == 0) {
			if (days_left == 1) printf("%d day", days_left);
			else printf("%d days", days_left);
		}
		else {
			if (days_left == 1) printf("%d day, ", days_left);
			else printf("%d days, ", days_left);
		}
	}

	if (hours_left != 0) {
		if (minutes_left == 0) {
			if (hours_left == 1) printf("%d hour", hours_left);
			else printf("%d hours", hours_left);
		}
		else {
			if (hours_left == 1) printf("%d hour, ", hours_left);
			else printf("%d hours, ", hours_left);
		}
	}

	if (minutes_left != 0) {
		if (minutes_left == 1) printf("%d minute", minutes_left);
		else printf("%d minutes", minutes_left);
	}
	printf("\n");
	return 0;
}
