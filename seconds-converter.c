#include <stdio.h>
#include <stdlib.h> // required for atoi
#include <string.h>

/*
 * this program takes an integer as input, treats this integer as a
 * number of seconds and calculates the equivalent amount of time
 * expressed in larger units (years, days, hours, minutes, seconds).
 */

void distribute_values(int *lesser, int *greater, int factor) {
  *greater += *lesser / factor;
  *lesser  -= *lesser / factor * factor;
}

int main(int argc, char* argv[]) {

  /*
   * the "simple" option is activated with the command line option -s
   * and makes the output shorter and more consistent length-wise by
   * printing every field with padded numbers and initials rather than
   * whole words (like H instead of hour(s).)
   */
	int simple = 0;
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "-s") == 0) {
			simple = 1;
			break;
		}
	}

  int seconds = simple ? abs(atoi(argv[2])) : abs(atoi(argv[1]));

	int minutes = 0;
	int hours   = 0;
	int days    = 0;
	int years   = 0;

  distribute_values(&seconds, &minutes, 60);
  distribute_values(&minutes, &hours,   60);
  distribute_values(&hours,   &days,    24);
  distribute_values(&days,    &years,   365);

	if (simple) {
		printf("%dY ",    years);
		printf("%03dD ",  days);
		printf("%02dH ",  hours);
		printf("%02dM ",  minutes);
		printf("%02dS\n", seconds);
	}

	else {
		if (years != 0) {
			if (days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
        printf(years == 1 ? "%d year" : "%d years", years);
			} else {
        printf(years == 1 ? "%d year, " : "%d years, ", years);
			}
		}

		if (days != 0) {
			if (hours == 0 && minutes == 0 && seconds == 0) {
        printf(days == 1 ? "%d day" : "%d days", days);
			} else {
        printf(days == 1 ? "%d day, " : "%d days, ", days);
			}
		}

		if (hours != 0) {
			if (minutes == 0 && seconds == 0) {
        printf(hours == 1 ? "%d hour" : "%d hours", hours);
			} else {
        printf(hours == 1 ? "%d hour, " : "%d hours, ", hours);
			}
		}

		if (minutes != 0) {
			if (seconds == 0) {
        printf(minutes == 1 ? "%d minute" : "%d minutes", minutes);
			} else {
        printf(minutes == 1 ? "%d minute, " : "%d minutes, ", minutes);
			}
		}

		if (seconds != 0) {
      printf(seconds == 1 ? "%d second" : "%d seconds", seconds);
		}
		printf("\n");
	}
}
