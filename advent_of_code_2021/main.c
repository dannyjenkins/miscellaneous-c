#include "days/day01.c"
#include "days/day02.c"
#include "days/day03.c"
#include "days/day04.c"
#include "days/day05.c"
#include "days/day07.c"

int main() {

	divider();
	day01calculation();
	
	divider();
	day02calculation();
	
	divider();
	day03calculation();
	
	divider();
	day04calculation();
	
	divider();
	day05calculation();

	divider();
	day07calculation();

	return 0;
}