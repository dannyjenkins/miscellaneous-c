#ifndef FUNCTIONS_H
#define FUNCTIONS_H

void divider();

/*
 * Return a numeric char's value as an int. '1' will return 1, for
 * example. Intended for use with chars '0' -> '9'.
 */
int cast(char input);

/*
 * Converts a char array of binary bits into a decimal int.
 */
int binary_to_decimal(int array_length, char input[]);

/*
 * Returns the absolute value of the input int.
 */
int abs(int input);

/*
 * Calculates the nth triangle number.
 */
int triangle_num(int n);

#endif
