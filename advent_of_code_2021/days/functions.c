#include <stdio.h>

void divider() {
	printf("-----\n");
}

int cast(char input) {
	return (int) (input - 48);
}

int binary_to_decimal(int array_length, char input[]) {
	int return_value = 0;
	int power = 1;

	for (int i = array_length - 1; i >= 0; i--) {
		if (input[i] == '1') return_value += power;
		power *= 2;
	}

	return return_value;
}

int abs(int input) {
	return input < 0 ? -input : input;
}

int triangle_num(int n) {
	return (n * n + n) / 2;
}