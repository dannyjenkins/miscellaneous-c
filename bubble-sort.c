#include <stdio.h>
#include <stdlib.h>

#define ASIZE 32 // Array size

void print_line(int char_number) {
	for (int i = 0; i < char_number; i++) printf("-");
	printf("\n");
}

void swap_values(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

void shuffle(int *array, size_t n) {
	if (n > 1) {
		size_t i;
		for (i = 0; i < n - 1; i++) {
			size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
			int t = array[j];
			array[j] = array[i];
			array[i] = t;
		}
	}
}

int is_sorted(int array[]) {
	for (int i = 0; i < ASIZE; i++) {
		if (i + 1 != array[i]) return 0;
	}
	return 1;
}

int main() {

	int array[ASIZE]; // Create array

	// Number of passes that have been performed so far
	int pass_count = 0;

	// Assign values to array
	for (int i = 0; i < ASIZE; i++) array[i] = i + 1;

	shuffle(array, ASIZE); // Shuffle array

	// Print array
	printf("Initial array:\n");
	for (int i = 0; i < ASIZE; i++) print_line(array[i]);
	printf("\n");

	/* Bubble sort, with each pass, moves the n'th largest value to
	its correct position. The largest value will be moved to its
	correct position in the first pass, the second largest value will
	be moved to its correct position in the second pass, and so on. */

	/* Therefore, we can use a variable to prevent the topmost values
	from being repeatedly and redundantly checked, since we already
	know that they are correctly sorted.  We will, in each pass, stop
	comparing values once the "sort ceiling" has been reached, since
	beyond this index in the array, there are only the largest values
	(which are correctly sorted). */
	int sort_ceiling = ASIZE - 1;

	// Check if array is sorted.
	// If not, perform one pass of Bubble sort
	while (!is_sorted(array)) {
		pass_count++;
		// Sort pass.
		for (int i = 0; i < sort_ceiling; i++) {
			if (array[i] > array[i + 1]) {
				swap_values(&array[i], &array[i + 1]);
			}
		}

		// Print array
		printf("Pass number %d:\n", pass_count);
		for (int i = 0; i < ASIZE; i++) {
			print_line(array[i]);
		}

		/* Print newline before moving on to the next pass; if the
		   final pass has been performed, then do not print a
		   newline */
		if (!is_sorted(array)) printf("\n");

		sort_ceiling--;
	}

	return 0;
}