#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(int argc, char* argv[]) {

  if (argc != 3) {
    printf("ERROR: Please specify two integer arguments\n");
    return 1;
  }

  for (int i = 1; i <= 2; i++) {
    for (int j = 0; j < strlen(argv[i]); j++) {
      if (!isdigit(argv[i][j])) {
        printf("ERROR: Please specify two integer arguments\n");
        return 1;
      }
    }
  }

  time_t t;
  srand((unsigned) time(&t));

  int rows = atoi(argv[1]);
  int cols = atoi(argv[2]);

  int tiles[rows][cols];

  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      tiles[row][col] = rand() % 2;
    }
  }

  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      printf(tiles[row][col] == 1 ? "\x1b[6;30;47m  \x1b[0m" : "  ");
    }
    printf("\n");
  }

  return 0;
}