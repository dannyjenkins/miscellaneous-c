// to compile with OpenMP:
// gcc -fopenmp prime.c

#include <stdio.h>
#include <stdlib.h>
#include <omp.h> // OpenMP

int is_prime(int the_number) {
	if (the_number == 1 || the_number == 0) return 0;
	else {
		for (int check = 2; check <= the_number/2; check++) {
			if (the_number % check == 0) return 0;
		}
	}
	return 1;
}

int get_digits(int the_number) {
	if (the_number >= 1000000000) return 10;
	if (the_number >= 100000000)  return 9;
	if (the_number >= 10000000)   return 8;
	if (the_number >= 1000000)    return 7;
	if (the_number >= 100000)     return 6;
	if (the_number >= 10000)      return 5;
	if (the_number >= 1000)       return 4;
	if (the_number >= 100)        return 3;
	if (the_number >= 10)         return 2;
	return 1;
}

int main(int argc, char* argv[]) {

	int lower_limit;
	int upper_limit;
	int divisors = 0;
	int nth_prime = 1;

	if (argc < 2) {
		printf("ERROR: Please provide an integer argument.\n");
		exit(1);
	}

	if (argc >= 2) {
		lower_limit = atoi(argv[1]);
	}

	if (argc > 3) {
		printf("ERROR: Too many arguments.\n");
		exit(1);
	}

	// if there is only one argument, check if this argument is prime.
	// if it's not prime, print out its factors
	if (argc == 2) {
		if (lower_limit < 0) {
			printf("ERROR: Input must be a positive number.\n");
			exit(1);
		}

		if (is_prime(lower_limit) == 1) {
			printf("%d is a prime number.\n", lower_limit);
		}

		else {
			if (lower_limit != 1 && lower_limit != 0) {
				// variable used to check when the multiplications
				// start mirroring, to avoid redundance such as
				// printing both 3 * 5 and 5 * 3
				int previous_multiplier;

				int indent = get_digits(lower_limit);

				// print factors
				for (int x = 2; x <= lower_limit / 2; x++) {
					if (lower_limit % x == 0) {
						// stops the loop once all unique factor pairs
						// have been used
						if (x == previous_multiplier) break;

						printf("%*d", indent, x);
						printf(" * %*d ", indent, lower_limit / x);
						printf("= %d\n", lower_limit);
						divisors++;
						previous_multiplier = lower_limit / x;
					}
				}

				// print prime factors
				int check = 2;
				int original_number = lower_limit;
				printf("Prime factors: ");
				while (check <= lower_limit) {
					if (lower_limit % check == 0) {
						printf("%d * ", check);
						lower_limit = lower_limit / check;
						check = 2;
					}
					else check++;
				}

				printf("\b\b\b = %d\n", original_number);
			}

			// special case for 1 and 0
			else {
				printf("%d is not a prime number.\n", lower_limit);
			}
		}
	}

	// if there are two arguments, check for and output primes between
	// and including these two arguments
	else {
		upper_limit = atoi(argv[2]);
		#pragma omp parallel for // use several threads
		for (int x = lower_limit; x <= upper_limit; x++) {
			if (is_prime(x) == 1) {
				printf("%d: %d\n", nth_prime, x);
				nth_prime++;
			}
		}
	}
	return 0;
}
